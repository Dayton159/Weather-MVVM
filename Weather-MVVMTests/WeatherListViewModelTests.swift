//
//  WeatherListViewModelTests.swift
//  Weather-MVVMTests
//
//  Created by Dayton on 16/06/21.
//

import XCTest
@testable import Weather_MVVM

class WeatherListViewModelTests: XCTestCase {
    
    private var weatherListVM: WeatherListViewModel!

    override func setUp() {
        super.setUp()
        weatherListVM = WeatherListViewModel()
        
        let firstWeather = Weather(temp: Dynamic<Double>(32.0), tempMin: Dynamic<Double>(20.0), tempMax: Dynamic<Double>(40.0))!
        let firstname = Dynamic<String>("Houston")
        
        let secondWeather = Weather(temp: Dynamic<Double>(72.0), tempMin: Dynamic<Double>(30.0), tempMax: Dynamic<Double>(80.0))!
        let secondname = Dynamic<String>("Boston")
        
        
        self.weatherListVM.addWeatherViewModel(WeatherViewModel(weather: WeatherResponse(name: firstname, main: firstWeather)!))
        
        self.weatherListVM.addWeatherViewModel(WeatherViewModel(weather: WeatherResponse(name: secondname, main: secondWeather)!))
    }
    
    func test_convertToCelcius() {
        let celcius = [0, 22.2222]
        
        self.weatherListVM.updateUnit(to: .celcius)
        
        self.weatherListVM.weatherViewModels.enumerated().forEach { index, viewModel in
            XCTAssertEqual(round(viewModel.temperature), round(celcius[index]))
        }
    }
    
    override func tearDown() {
        super.tearDown()
    }
}
