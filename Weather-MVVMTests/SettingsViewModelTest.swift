//
//  SettingsViewModelTest.swift
//  Weather-MVVMTests
//
//  Created by Dayton on 16/06/21.
//

import XCTest
@testable import Weather_MVVM

class SettingsViewModelTest: XCTestCase {
    
    private var settingsVM: SettingsViewModel!
    let userDefaults = UserDefaults.standard
    
    override func setUp() {
        super.setUp()
        
        self.settingsVM = SettingsViewModel()
    }
    
    func test_temperatureDisplayName() {
        XCTAssertEqual(self.settingsVM.selectedUnit?.displayName, "Fahrenheit")
    }
    
    func test_temperatureDefaultValue() {
        
        XCTAssertEqual(settingsVM.selectedUnit, .fahrenheit)
    }
    
    func test_saveUserUnitSelection() {
        self.settingsVM.selectedUnit = .celcius
        
        XCTAssertNotNil(userDefaults.value(forKey:"unit"))
    }
    
    override func tearDown() {
        super.tearDown()
        
        userDefaults.removeObject(forKey: "unit")
    }
    
}
