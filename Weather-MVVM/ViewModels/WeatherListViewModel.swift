//
//  WeatherListViewModel.swift
//  Weather-MVVM
//
//  Created by Dayton on 14/06/21.
//

import Foundation

class WeatherListViewModel {
    var weatherViewModels = [WeatherViewModel]()
    
    func addWeatherViewModel(_ vm: WeatherViewModel) {
        weatherViewModels.append(vm)
    }
    
    func numberOfRows(_ section:Int) -> Int {
        weatherViewModels.count
    }
    
    func modelAt(_ index:Int) -> WeatherViewModel {
        return weatherViewModels[index]
    }
    
    func updateUnit(to unit:Unit) {
        switch unit {
            case .celcius:
                toCelcius()
            case .fahrenheit:
                toFahrenheit()
        }
    }
    
    private func toCelcius() {
       weatherViewModels = weatherViewModels.map { vm in
            let weatherVM = vm
            weatherVM.temperature = (weatherVM.temperature - 32) * 5 / 9
            return weatherVM
        }
    }
    
    private func toFahrenheit() {
        weatherViewModels = weatherViewModels.map { vm in
             let weatherVM = vm
             weatherVM.temperature = (weatherVM.temperature * 9 / 5) + 32
             return weatherVM
         }
    }
}

class WeatherViewModel {
    let weather: WeatherResponse
    var temperature:Double
    
    init(weather: WeatherResponse) {
        self.weather = weather
        self.temperature = weather.main.temp.value
    }
    
    var city:String {
        weather.name.value
    }
}

//MARK: - Type Eraser
// (used for binding ViewModel To View, We can say type eraser replace default type to custom type) ---
class Dynamic<T>:Decodable where T:Decodable {
    
    typealias Listener = (T) -> Void // Creating dynamic type which return type T.
    
    var listener:Listener? //Create object of dynamic type
    
    var value:T { // Save the value to Dynamic Type.
        didSet {
            //firing value to listener once it gets sets
            listener?(value)
        }
    }
    
    //also need to allows UI to bind to this particular value first
    //this func will used by VC to bind viewmodel to certain property
    func bind(listener: @escaping Listener) {
        self.listener = listener
        self.listener?(self.value)
    }
    
    
    init(_ value:T) {
        self.value = value
    }
    
    private enum CodingKeys:CodingKey {
        case value
    }
}
