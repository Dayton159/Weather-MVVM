//
//  SettingsViewModel.swift
//  Weather-MVVM
//
//  Created by Dayton on 14/06/21.
//

import Foundation

enum Unit:String, CaseIterable {
    case celcius = "metric"
    case fahrenheit = "imperial"
    
    var displayName: String {
        switch self {
        case .celcius:
            return "Celcius"
        case .fahrenheit:
            return "Fahrenheit"
        }
    }
}

class SettingsViewModel {
    
    let units = Unit.allCases
    
    var selectedUnit:Unit? {
        get {
            let userDefaults = UserDefaults.standard
            if let value  = userDefaults.value(forKey: "unit") as? String {
                return Unit(rawValue: value)
            } else {
                userDefaults.set(Unit.fahrenheit.rawValue, forKey: "unit")
                return Unit(rawValue: Unit.fahrenheit.rawValue)
            }
            
        }set {
            let userDefaults = UserDefaults.standard
            userDefaults.set(newValue?.rawValue, forKey: "unit")
        }
    }
    
}
