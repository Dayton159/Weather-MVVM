//
//  AddWeatherViewModel.swift
//  Weather-MVVM
//
//  Created by Dayton on 14/06/21.
//

import Foundation

class AddWeatherViewModel {
    
    var city:String = ""
    var state:String = ""
    var zipCode:String = ""
    
    func addWeather(for city: String, completion: @escaping(WeatherViewModel) -> Void) {
        
        let weatherURL = Constants.Urls.urlForWeatherByCity(city: city)
        
        let weatherResource = Resource<WeatherResponse>(url: weatherURL) { data in
            
            let weatherResponse = try? JSONDecoder().decode(WeatherResponse.self, from: data)
            return weatherResponse
        }
        
        WebService.load(resource: weatherResource) { result in
            
            if let weatherResponse = result {
                let viewModel = WeatherViewModel(weather: weatherResponse)
                completion(viewModel)
            }
            
        }
        
    }
}
