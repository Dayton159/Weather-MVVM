//
//  BindingTextField.swift
//  Weather-MVVM
//
//  Created by Dayton on 14/06/21.
//

import UIKit

class BindingTextField:UITextField {
    
    var textChangeClosure: (String) -> Void = { _ in }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func bind(callback: @escaping (String) -> Void) {
        self.textChangeClosure = callback
    }
    
    private func commonInit() {
        self.addTarget(self, action: #selector(textFieldChange), for: .editingChanged)
    }
    
    @objc func textFieldChange(_ textField: UITextField) {
        guard let text = textField.text else { return }
        self.textChangeClosure(text)
    }
    
}
