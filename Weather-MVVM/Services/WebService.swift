//
//  WebService.swift
//  Weather-MVVM
//
//  Created by Dayton on 10/06/21.
//

import Foundation


struct Resource<T> {
    let url: URL
    let parse: (Data) -> T?
}


final class WebService {
    
    static func load<T>(resource: Resource<T>, completion: @escaping (T?) -> ()) {
        
        URLSession.shared.dataTask(with: resource.url) { data, response, error in
            
         
            
            guard let data = data, error == nil else {
                completion(nil)
                return
            }
            
            DispatchQueue.main.async {
                completion(resource.parse(data))
            }
           
        }.resume()
    }
}
