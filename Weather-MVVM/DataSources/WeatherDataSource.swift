//
//  WeatherDataSource.swift
//  Weather-MVVM
//
//  Created by Dayton on 15/06/21.
//

import UIKit

class WeatherDataSource: NSObject, UITableViewDataSource {
    
    private let cellIdentifier = "WeatherCell"
    private var weatherListVM: WeatherListViewModel
    
    init(_ weatherListVM:WeatherListViewModel) {
        self.weatherListVM = weatherListVM
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.weatherListVM.numberOfRows(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as? WeatherCell else { return UITableViewCell() }
        
        let weatherVM = weatherListVM.modelAt(indexPath.row)
        
        cell.configure(weatherVM)
        return cell
    }
    
    
}
