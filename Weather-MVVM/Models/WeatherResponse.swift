//
//  WeatherResponse.swift
//  Weather-MVVM
//
//  Created by Dayton on 14/06/21.
//

import Foundation

struct WeatherResponse:Decodable {
    let name:Dynamic<String>
    let main: Weather
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = Dynamic(try container.decode(String.self, forKey: .name))
        self.main = try container.decode(Weather.self, forKey: .main)
    }
    
    private enum CodingKeys: CodingKey {
        case name, main
    }
    
    
}

extension WeatherResponse {
    init?(name: Dynamic<String>, main: Weather) {
        self.name = name
        self.main = main
    }
}

struct Weather:Decodable {
    
    let temp:Dynamic<Double>
    let tempMin:Dynamic<Double>
    let tempMax:Dynamic<Double>
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        temp = Dynamic(try container.decode(Double.self, forKey: .temp))
        tempMin = Dynamic(try container.decode(Double.self, forKey: .tempMin))
        tempMax = Dynamic(try container.decode(Double.self, forKey: .tempMax)) 
    }
    
    private enum CodingKeys:String, CodingKey {
        case temp
        case tempMin = "temp_min"
        case tempMax = "temp_max"
    }
    
    init?(temp: Dynamic<Double>, tempMin: Dynamic<Double>, tempMax:Dynamic<Double>) {
        self.temp = temp
        self.tempMin = tempMin
        self.tempMax = tempMax
    }
    
}
