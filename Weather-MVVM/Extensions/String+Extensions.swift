//
//  String+Extensions.swift
//  Weather-MVVM
//
//  Created by Dayton on 14/06/21.
//

import Foundation

extension String {
    
    func escaped() -> String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? self
    }
}
