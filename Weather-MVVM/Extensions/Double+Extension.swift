//
//  Double+Extension.swift
//  Weather-MVVM
//
//  Created by Dayton on 14/06/21.
//

import Foundation

extension Double {
    
    func formatAsDegree() -> String {
        return String(format: "%.0f°", self)
    }
}
