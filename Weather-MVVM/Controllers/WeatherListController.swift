//
//  WeatherListController.swift
//  Weather-MVVM
//
//  Created by Dayton on 01/06/21.
//

import Foundation
import UIKit

class WeatherListController: UITableViewController {
    
    private var weatherListViewModel = WeatherListViewModel()
    private var dataSource:TableViewDataSource<WeatherCell, WeatherViewModel>?
    private var lastUnitSelection: Unit!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      configureNavigationBar(withTitle: "Weather", prefersLargeTitles: true)
      setlastSelection()
      configureTableView()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddWeatherCityVC" {
           addWeatherCitySegue(segue: segue)
        } else if segue.identifier == "SettingsTableViewController" {
            addSettingSegue(segue: segue)
        } else if segue.identifier == "WeatherDetailViewController" {
            addWeatherDetailSegue(segue: segue)
        }
    }
    
    //MARK: - Helpers
    
    private func configureTableView() {
        self.dataSource = TableViewDataSource(cellIdentifier: "WeatherCell", items: weatherListViewModel.weatherViewModels) { cell, viewModel in
            
            cell.configure(viewModel)
        }
        self.tableView.dataSource = dataSource
    }
    
    private func setlastSelection() {
        let userDefault = UserDefaults.standard
        if let value = userDefault.value(forKey: "unit") as? String {
            self.lastUnitSelection = Unit(rawValue: value)!
        }
    }
    
    private func addWeatherCitySegue(segue:UIStoryboardSegue) {
        guard let navbar = segue.destination as? UINavigationController,
              let addWeatherCityVC = navbar.viewControllers.first as? AddWeatherCityViewController
              else { fatalError("Controller not found")}
        
        addWeatherCityVC.delegate = self
    }
    
    private func addSettingSegue(segue:UIStoryboardSegue) {
        guard let navbar = segue.destination as? UINavigationController,
              let settingVC = navbar.viewControllers.first as? SettingsTableViewController
              else { fatalError("Controller not found")}
        
        settingVC.delegate = self
    }
    
    private func addWeatherDetailSegue(segue: UIStoryboardSegue) {
        
        guard let weatherDetailVC = segue.destination as? WeatherDetailViewController,
              let indexPath = self.tableView.indexPathForSelectedRow else { return }
        
        let weatherVM = self.weatherListViewModel.modelAt(indexPath.row)
        weatherDetailVC.weatherViewModel = weatherVM
    }
}

//MARK: - Extensions

extension WeatherListController: AddWeatherDelegate {
    func addWeatherDidSave(viewModel: WeatherViewModel) {
        weatherListViewModel.addWeatherViewModel(viewModel)
        self.dataSource?.updateItems(self.weatherListViewModel.weatherViewModels)
        self.tableView.reloadData()
    }
    
}

extension WeatherListController: SettingsDelegate {
    func settingsDone(vm: SettingsViewModel) {
        if let selectedUnit = vm.selectedUnit,
           lastUnitSelection.rawValue != vm.selectedUnit?.rawValue {
            weatherListViewModel.updateUnit(to: selectedUnit)
            tableView.reloadData()
            lastUnitSelection = vm.selectedUnit
        }
    }
    
}
