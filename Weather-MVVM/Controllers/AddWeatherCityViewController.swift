//
//  AddWeatherCityViewController.swift
//  Weather-MVVM
//
//  Created by Dayton on 01/06/21.
//

import Foundation
import UIKit

protocol AddWeatherDelegate: AnyObject {
    func addWeatherDidSave(viewModel: WeatherViewModel)
}

class AddWeatherCityViewController:UIViewController {
    
    private var addWeatherVM = AddWeatherViewModel()
    
    //MARK: - Bindings
    @IBOutlet weak var cityNameTextField:BindingTextField! {
        didSet {
            // whenever we start typing the text ($0) in cityTextField is going to be assigned to city property
            cityNameTextField.bind { self.addWeatherVM.city = $0 }
        }
    }
    @IBOutlet weak var stateNameTextField:BindingTextField! {
        didSet {
            stateNameTextField.bind { self.addWeatherVM.state = $0 }
        }
    }
    @IBOutlet weak var zipCodeTextField:BindingTextField! {
        didSet {
            zipCodeTextField.bind { self.addWeatherVM.zipCode = $0 }
        }
    }
    
    weak var delegate:AddWeatherDelegate?
    
    @IBAction func saveCityButtonPressed() {
        
        if let city = cityNameTextField.text {
            
            addWeatherVM.addWeather(for: city) { weatherViewModel in
                
                self.delegate?.addWeatherDidSave(viewModel: weatherViewModel)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar(withTitle: "Add City", prefersLargeTitles: false)
       
    }
}
