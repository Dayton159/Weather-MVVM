//
//  WeatherDetailViewController.swift
//  Weather-MVVM
//
//  Created by Dayton on 15/06/21.
//

import UIKit

class WeatherDetailViewController: UIViewController {
    
    @IBOutlet weak var cityNameLabel:UILabel!
    @IBOutlet weak var currentTempLabel:UILabel!
    @IBOutlet weak var maxTempLabel:UILabel!
    @IBOutlet weak var minTempLabel:UILabel!
    
    var weatherViewModel: WeatherViewModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindsUI()
    }
    
    private func bindsUI() {
        if let weatherVM = weatherViewModel {
            weatherVM.weather.name.bind { self.cityNameLabel.text = $0}
            weatherVM.weather.main.temp.bind { self.currentTempLabel.text = $0.formatAsDegree()}
            weatherVM.weather.main.tempMin.bind { self.minTempLabel.text = $0.formatAsDegree()}
            weatherVM.weather.main.tempMax.bind { self.maxTempLabel.text = $0.formatAsDegree()}
        }
        
        //Test Change value temporary
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.weatherViewModel?.weather.name.value = "Dayton Hsu"
        }
    }
}
