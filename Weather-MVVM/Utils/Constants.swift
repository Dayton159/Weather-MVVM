//
//  Constants.swift
//  Weather-MVVM
//
//  Created by Dayton on 14/06/21.
//

import Foundation

struct Constants {
    
    struct Urls {
        static func urlForWeatherByCity(city:String) -> URL {
            
            let userDefaults = UserDefaults.standard
            let unit = (userDefaults.value(forKey: "unit") as? String) ?? "imperial"
            
            guard let weatherUrl = URL(string: "https://api.openweathermap.org/data/2.5/weather?appid=356cdbbe244ee433f9b8adb88298544f&units=\(unit)&q=\(city.escaped())") else { fatalError("URL is incorrect")}
            
            return weatherUrl
        }
    }
    
}
